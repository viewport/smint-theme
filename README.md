Scroll Viewport
===============
Smint Theme
---------------

![Viewport Smint Theme](https://bytebucket.org/viewport/smint-theme/raw/8041150fd54ab59b27fdd2d01e60c6c6fcd4052a/img/thumbnail.png) 

The Clean Theme for Scroll Viewport is an example theme based on Twitter's bootstrap.

In order to install this theme 

1. With viewport already installed in your system enable FTP through the admin console. 
1. With any FTP client connect to the system and create a new top-level folder - "smint-theme" for example.
1. Upload every file from this folder.

For more information please visit the [Scroll Viewport documentation](http://www.k15t.com/display/VPRT/Documentation). 

Features
--------

* Slideshow support
* Home / Children Pages 
* Macros Support
* One Site Page


Need Help?
----------

If you have any questions please visit 

* [Viewport Documentation Pages](http://www.k15t.com/display/VPRT/Documentation)
* [Scroll Viewport Developers Group](https://groups.google.com/forum/#!forum/scroll-viewport-dev).

Copyright  
---------

K15t Software GmbH [@k15t](http://twitter.com/k15tsoftware)
Licensed under the Apache License, Version 2.0 

Third party
---------------------------------------------------

Smint               MIT     Created by [Robert McCracken](http://www.outyear.co.uk/smint/)

Cycle2              MIT     Created by [M. Alsup](http://jquery.malsup.com/cycle2/)